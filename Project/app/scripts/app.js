'use strict';
/*global Firebase*/
var App = angular.module('projectApp', [
  'ngSanitize',
  'ngRoute',
  'angularfire.firebase',
  'angularfire.login',
  'firebase'
]);
App.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/main.tpl',
      controller: 'MainCtrl'
    })
    .when('/detail/:id',{ // Sends the id of the specific deal.
      templateUrl : 'views/detail.tpl',
      controller : 'DetailCtrl'
    })
    .when('/newDeal/',{
      templateUrl : 'views/newDeal.tpl',
      controller : 'NewDealCtrl'
    })
    .when('/login', {
        authRequired: false, // if true, must log in before viewing this page
        templateUrl: 'views/login.html',
        controller: 'LoginController'
      })
      .otherwise({
        redirectTo: '/'
      });
});
// When the program is first launched, these settings are set before the program is loaded. Creating a login with facebook and setting the rootscope variables.
App.run(['$firebaseSimpleLogin', '$rootScope', function ($firebaseSimpleLogin, $rootScope){

  // This is launched when the app starts to use the facebook login.
  var fb = new Firebase('https://blazing-fire-6418.firebaseio.com/');
  $rootScope.loginObject = $firebaseSimpleLogin(fb);

  // This is the default state for the successful submission message that is displayed when a new entry is made
  $rootScope.showAlert = false;

}]);