'use strict';
/* global Firebase */
/* global App */
App.factory('FireConn', ['$firebase', function ($firebase) {
	var url = 'https://blazing-fire-6418.firebaseio.com/Discount',
		ref = new Firebase(url);
	return $firebase(ref);
}]);