'use strict';
/* global App */
App.controller('NewDealCtrl', ['$scope', 'FireConn', '$location', '$rootScope', function ($scope, FireConn, $location, $rootScope){
// creates the connection to save to the database
	$scope.restaurant = FireConn;

// This is used when the user clicks the save button on the newDeal page.
  $scope.saveData = function(){
	// These variables set the base values that are not user input for each new entry
	  $scope.discount.user = $scope.loginObject.user.displayName;
	  $scope.discount.userlink = $scope.loginObject.user.profileUrl;
	  $scope.discount.success = 0;
	  $scope.discount.fail = 0;
	  $scope.discount.perc = 100;
	  $scope.discount.comments = {'newstart':true};

	// This adds it to the database
	  $scope.restaurant.$add($scope.discount);

	// A reset for the fields that the user inputs so that they are not filled with previous information if they decide to go back and enter a second deal
	  $scope.discount.restaurant = '';
	  $scope.discount.address = '';
	  $scope.discount.info = '';

	// Sends the user back to the main page
	  $location.path('/');

	// This thanks them for making an entry into the system on the main page.
	  $rootScope.showAlert = true;
	};

}]);