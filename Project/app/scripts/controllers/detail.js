'use strict';
/* global App */
/* global Firebase */
App.controller('DetailCtrl',[ '$scope', '$routeParams', 'FireConn', '$firebase', function ($scope, $routeParams, FireConn, $firebase){

  // grabbing the appropriate data from the firebase to set the variables in the template.
    $scope.deal = FireConn.$child($routeParams.id);

  // grabbing the id from the routeparams to use for the appending to the comments DB
    var id = $routeParams.id;

  // Creating a firebase connection preset to using the ID and setting things into the comments section. Also binding the params so they instantly update the fields in the comment section.
    var conne = new Firebase('https://blazing-fire-6418.firebaseio.com/Discount/'+id+'/comments/');
    $scope.comments = $firebase(conne);
    $firebase(conne).$bind($scope, 'opinion');

  // When comments are submitted through the form, it grabs the user's data to link to the comment
    $scope.newComment = function(){
      $scope.talk.user = $scope.loginObject.user.displayName;
      $scope.talk.userlink = $scope.loginObject.user.profileUrl;
      $scope.opinion.$add($scope.talk);
      $scope.talk = '';
    };

  // Whenever the Yes is selected if the deal worked for them, it increments the page, saves it to the db, calculates the new percentage of yes vs no and sets the color of the percentage to true for the color swap
    $scope.dealSucc = function(){
      $scope.deal.success++;
      $scope.deal.$save('success');
      $scope.deal.perc = $scope.deal.success / ($scope.deal.success + $scope.deal.fail) * 100 + 0;
      $scope.deal.$save('perc');
      $scope.upvote = true;
    };

  // Whenever the No is selected if the deal worked for them, it increments the page, saves it to the db, calculates the new percentage of yes vs no and sets the color of the percentage to false for color swap
    $scope.dealFail = function(){
      $scope.deal.fail++;
      $scope.deal.$save('fail');
      $scope.deal.perc = $scope.deal.success / ($scope.deal.success + $scope.deal.fail) * 100 + 0;
      $scope.deal.$save('perc');
      $scope.upvote = false;

    };

  }]);