'use strict';
/*global App*/
App.controller('MainCtrl', ['$scope','FireConn','$location', function ($scope, FireConn,$location) {
	// Pulls the data from the db using the firebase factory, this makes the entries available for the main page
	$scope.item = FireConn;

	// This function is so that the user can select the item anywhere in the box that contains it and it will take them to the detail page
	$scope.goToDetail = function(id)
	{
		$location.path('/detail/'+id);
	};
	// Sets the base scope for angular, generated by Yeoman.
	$scope.awesomeThings = [
		'HTML5 Boilerplate',
		'AngularJS',
		'Karma'
	];
}]);
// Because my percentages were coming back with many decimal places, this filter is used to round it to the nearest whole number.
App.filter('percentFilt', function percentFilt(){

	return function(data){
		var d = data.toFixed(0);
		return d;
	};
});
