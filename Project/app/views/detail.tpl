<!-- Page is only shown if the user is logged in. Otherwise it will default to the facebook login page -->
<div ng-show="loginObject.user" class="row marketing mainbox">
	<article class="col-sm-12 col-md-12 col-lg-12">

<!-- Panel info to display deal info, brought in using the ID through the DB-->
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">{{deal.info}}</h3>
      </div>
      <div class="panel-body">
        <p>{{deal.restaurant}}</p>
        <p>{{deal.address}}</p>
      </div>
    </div>
    <div class="pull-right">
      <p>Submitted by: <a href="{{deal.userlink}}">{{deal.user}}</a></p>
    </div>
    <!-- Section on if the deal worked or not -->
    <footer class="clearfix"></br></br>
      <p>Did this deal work for you?</p>
       <div class="pull-left">
        <button class="pull-left btn btn-danger" ng-click="dealFail()">No</button>
        <button class="pull-left btn btn-success successbtn" ng-click="dealSucc()">Yes</button>
      </div>
      <div class="pull-left">
        <p class="successrate">Success Rate: <span ng-class="{'success':upvote === true,  'fail':upvote === false}">{{ deal.perc | percentFilt }}&#37;</span> </p>
      </div>
    </footer>
	</article>

<!-- Comment Section-->
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 commentarea">

    <h4>Comments</h4>
    <!-- Looped through the DB using the ID from the routeParams -->
    <div ng-repeat="($id, comment) in comments">
        <p><a href="{{comment.userlink}}">{{comment.user}}</a> {{comment.opinion}}</p>
    </div>
    <!-- Used for adding comments to the page -->
    <div class="row form-group newcommentarea">
      <label for='opinion' class='col-sm-3 col-md-3 col-lg-3 control-label'>New Comment</label>
      <div class="col-sm-9 col-md-9 col-lg-9">
        <textarea rows="3" class="form-control" name='opinion' id='opinion' ng-model="talk.opinion" placeholder='Add a comment here.'></textarea>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button class="btn btn-success pull-right newcomment" ng-click="newComment()">Submit Comment</button>
     </div>
    </div>
  </div>
</div>
