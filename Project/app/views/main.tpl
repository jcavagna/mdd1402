

<!-- This is the alert given when a user successfully enters a new deal-->
<div class="alert alert-success alert-dismissable ol-xs-12 col-sm-12 col-md-12 col-lg-12" ng-show="showAlert">
    <button type="button" class="close" ng-click="showAlert = false">&times;</button>
    <p><strong>Success!</strong> Thank you for the deal submission.</p>
</div>

<!-- Main page is only shown if the user is logged in. If no user it shows the facebook login page-->
<div ng-show="loginObject.user" class="mainbox">
  <!-- The filter for the list of deals. -->
  <div id="foodfilter" class="row">
    <label for='type' class='col-xs-12 col-sm-12 col-md-4 col-lg-4 control-label'>Filter by Type of Food</label>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
      <select class="form-control" ng-model="typefilt" name="typefilt" id="typefilt">
          <option ng-options="" value="">All</option>
          <option value="deli">Deli</option>
          <option value="fast food">Fast Food</option>
          <option value="coffee">Coffee</option>
          <option value="american">American</option>
          <option value="asian">Asian</option>
          <option value="mexican">Mexican/Latin</option>
          <option value="other">Other</option>
      </select>
    </div>
  </div>
  <p class="col-xs-12 col-sm-12 col-md-12 col-lg-12 infotext row">Select a deal below for more information</p>

<!-- This is the list of deals shown. The filter is set to all by default -->
<div id="deallist" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row">
  <ul class="list-group" ng-repeat="($id, deal) in filttype = (item | orderByPriority | filter:typefilt)">
    <li class="list-group-item suplist" ng-click="goToDetail(deal.$id)">
      <h4>{{deal.restaurant}}</h4><span class="badge">{{deal.perc | percentFilt }}&#37;</span>
      <p>{{deal.info}}</p>
    </li>
  </ul>

</div>
<!-- If there are no deals in the filtered category, this message is shown -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row">
  <p ng-hide="filttype.length">No deals in this category!</p>
</div>
