<!-- This form will not show unless the user is logged in. If no user, then it will show the default login with facebook page -->

<form class="form-horizontal mainbox" role="form" ng-show="loginObject.user">
	<!-- This form is used to gather the user input fields for a new deal-->

	<!-- The Restaurant name field-->
	<div class="form-group">
		<label for='name' class='col-sm-3 col-md-3 col-lg-3 control-label'>Restaurant Name</label>
		<div class="col-sm-9 col-md-9 col-lg-9">
			<input type="text" class="form-control" required name='name' id='name' ng-model="discount.restaurant" placeholder='Restaurant Name'/>
		</div>
	</div>

	<!-- The food genre type selector-->
	<div class="form-group">
		<label for='type' class='col-sm-3 col-md-3 col-lg-3 control-label'>Type of Food</label>
		<div class="col-sm-9 col-md-9 col-lg-9">
			<select class="form-control" required ng-model="discount.type" name="type" id="type">
  				<option> - Please Select - </option>
				<option value="deli">Deli</option>
				<option value="fast food">Fast Food</option>
				<option value="coffee">Coffee</option>
				<option value="american">American</option>
				<option value="asian">Asian</option>
				<option value="mexican">Mexican/Latin</option>
				<option value="other">Other</option>
  			</select>
		</div>
	</div>

	<!-- The address for the restaurant -->
	<div class="form-group">
		<label for='address' class='col-sm-3 col-md-3 col-lg-3 control-label'>Address</label>
		<div class="col-sm-9 col-md-9 col-lg-9">
			<textarea rows="3" class="form-control" name='address' id='address' ng-model="discount.address" placeholder='Restaurant address - optional'></textarea>
		</div>
	</div>

	<!-- The deal information -->
	<div class="form-group">
		<label for='info' class='col-sm-3 col-md-3 col-lg-3 control-label'>Deal Info</label>
		<div class="col-sm-9 col-md-9 col-lg-9">
			<textarea rows="3" class="form-control" required name='info' id='info' ng-model="discount.info" placeholder="When and what's on sale."></textarea>
		</div>
	</div>

	<!-- The submit button sends the function which gathers the data and grabs the user input and compiles it with the base variables for the new entry. After it redirects them to the main page with a thank you message. -->
	<button class="pull-right btn btn-success" ng-click="saveData()">Save</button>

</form>